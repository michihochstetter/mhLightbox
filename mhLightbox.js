/*! mhLightbox v3.9 | (c) Michael Hochstetter | https://gitlab.com/michihochstetter/mhLightbox/raw/master/LICENSE !*/
if (typeof jQuery == 'undefined') {
	if (typeof $ != 'function') {
		console.warn(
			'This Website needs a jQuery Library for mhLightbox to work.'
		);
		jQuery = function() {};
	} else {
		jQuery = $;
	}
}
(function($) {
	$.mhLightbox = function(options) {
		// --- START

		// settings
		var _settings = $.extend(
			{
				selector: '[mhlightbox]',
				imageTypes: 'jpg|jpeg|gif|png|svg|webp',
				imageMaxWidth: null, // (int) | null=>default
				imageMaxHeight: null, // (int) | null=>default
				imageEditActions: false, // (boolean) | false=>default
				padding: '80px', // (int) | px | %
				inlineCSS: '', // (string) | path to css
				initCallback: function(objects) {}, // array of all objects
				moveCallback: function(direction) {}, // previous, next
			},
			options || {}
		);

		// variables
		var objectInstance = $(_settings.selector);
		var pluginActive = false;
		var objects = {};
		var objectActive = '';
		var objectCount = 0;
		var objectGroupDefault = '_default';
		var imageTypesRegex = new RegExp(
			'\\.(' + _settings.imageTypes + ')(?:\\?.*)?$',
			'i'
		);
		var browserWidth = $(window).width();
		var browserHeight = $(window).height();
		var lbPadding = _settings.padding;

		var imgEdits = {
			rotation: 0,
			flip: 1,
			zoomLevel: 1,
			mouseDragX: 0,
			mouseDragY: 0,
		};
		
		// functions
		function createStructur() {
			if ($('#mhLightbox_container').length !== 1) {
				$('body').append("<div id='mhLightbox_container'>");
					$('#mhLightbox_container').append("<div id='mhLightbox_wrapper_gallery'>");
					$('#mhLightbox_container').append("<div id='mhLightbox_wrapper'>");
						$('#mhLightbox_wrapper').append("<img id='mhLightbox_object'>");
						$('#mhLightbox_wrapper').append("<div id='mhLightbox_close' title='schließen (ESC-Taste)'>");
						if(_settings.imageEditActions) {
							$('#mhLightbox_wrapper').append("<div id='mhLightbox_img_edit'>");
								$('#mhLightbox_img_edit').append("<div id='mhLightbox_img_edit_rotate'>");
								$('#mhLightbox_img_edit').append("<div id='mhLightbox_img_edit_flip'>");
							$('#mhLightbox_wrapper').append("<div id='mhLightbox_zoom'>");
								$('#mhLightbox_zoom').append("<div id='mhLightbox_zoom_icon_plus'>");
								$('#mhLightbox_zoom').append("<div id='mhLightbox_zoom_icon_minus' class='disabled'>");
						}
						$('#mhLightbox_wrapper').append("<div id='mhLightbox_panel'>");
							$('#mhLightbox_panel').append("<div id='mhLightbox_object_subtitle'>");
							$('#mhLightbox_panel').append("<div id='mhLightbox_object_controls'>");
								$('#mhLightbox_object_controls').append("<div id='mhLightbox_prev' title='vorheriges Bild (Pfeil-Links)'>");
								$('#mhLightbox_object_controls').append("<div id='mhLightbox_object_count'>");
								$('#mhLightbox_object_controls').append("<div id='mhLightbox_next' title='n&auml;chstes Bild (Pfeil-Rechts)'>");
									$('#mhLightbox_prev').append("<div id='mhLightbox_prev_icon'>");
									$('#mhLightbox_next').append("<div id='mhLightbox_next_icon'>");
			}
		}
		function createGallery(group, groupKey) {
			$('#mhLightbox_wrapper_gallery').html('');
			// load all objects from same group in gallery
			$.each(objects[group], function(index, element) {
				var object = objects[group][index];
				var objectGallerySrc = '';
				if (object.type == 'image') {
					objectGallerySrc = object.href;
				} else {
					// == video|content
					if (object.type == 'video') {
						objectGallerySrc =
							'data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAABkAAD/4QMvaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjYtYzE0MiA3OS4xNjA5MjQsIDIwMTcvMDcvMTMtMDE6MDY6MzkgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDQyAyMDE4IChXaW5kb3dzKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo2M0VERDc5M0NFRjExMUU4QkM4QkJCMzJEODhGQTUwMCIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo2M0VERDc5NENFRjExMUU4QkM4QkJCMzJEODhGQTUwMCI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjYzRURENzkxQ0VGMTExRThCQzhCQkIzMkQ4OEZBNTAwIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjYzRURENzkyQ0VGMTExRThCQzhCQkIzMkQ4OEZBNTAwIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+/+4ADkFkb2JlAGTAAAAAAf/bAIQAAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQICAgICAgICAgICAwMDAwMDAwMDAwEBAQEBAQECAQECAgIBAgIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMD/8AAEQgAZABLAwERAAIRAQMRAf/EALMAAQABBAMBAAAAAAAAAAAAAAAKBgcICwQFCQMBAQAABgMBAAAAAAAAAAAAAAADBQYHCAkBBAoCEAAABQMDAwIEBAYDAAAAAAABAgMEBQAGBxESCCETCTEKQWEUFSJygiNRcZEyQlK1NnYRAAEDAgMEBQcHBBMAAAAAAAEAAgMEBREGByExEghBgSITCVFhcZGxMrKhwUJSksIUYnJTlNHSg5Ozw9M0VHSE1BU1VZXVFhf/2gAMAwEAAhEDEQA/AJ/FESiJREoiURKIlESiJREoiURKIlESiJREoiURKIlESiJREoiURKIlESiJREoiURKIlEWOvIjlrxv4nQDC5OQ2XbUxjHyx104VrLqvZC4J0Wvb+rNA2nAM5a6ZxNl3Sd87RmsRAVCbxLvLrJrxmCy2CITXiojgY7cDiXOw38LGgudh04A4dKunpbolqvrVc5LTpdY6271UABldEGMhh4seHvqmZ0dPCX4HgEkrS/B3CDwnDzSn/cF+N6HVUTjr0yfdZCCIFXgMU3E2SV0HQBTC6DW2uAG9Q3kL89KoqXVzJkZwZLPIPyYnD4uFZa2zwxOa6vYH1Vvs9E4/RmuMDiPT+H78eolW2fe5E4DNDmK3s3kzJgAhooxx7YKZD/MoSWWI9QAD5lCuk7WfKbTsjrXeiOP55QqspvCi5lp2gy1+UYT5H1tYSP3u2vHyrpR9yzwUARAMU8szAAiAGCxMP6GAB9Q3Z2KbQfmADUP/ANqyr/R7h9iH+XUwHhK8xxGJvWSR/bLn/wAOuQh7lPggsIApjPlW1DdpqvYWKTAAaa7h+lza5Hbr09NflXI1pyqd8FeP3OL5pioUnhM8yEY7N3yW/Z0Vlx+9aWqr4v3F3j2f7Pq22e4Td6/dMaw6vb/P9lvWX1/Tursx6x5Qf7wq2+mMfdeVI6zwr+aCmx7l+WqjD9HXyjH0d7SRfLgsj8V+avxu5Zmo63IzkKztCclXCLVk0yVaV5WJHGWXMBCFc3XNwSNlsA3iBdXEkkGo9NanNBqZky4SthZWCOVxwAkY9g63ubwDrcFanOfh/c1+SaCW61eV5K63QtLnuoKmlrH4DaeGmhmNU/ZtwZA4r1QRWScJJLoKproLpkWRWROVRJZJQoHTVSUIJiKJqEMAlMAiAgOoVXYIIxG0FYaPY+J5jkBbI0kEEYEEbCCDtBB2EFfSuV8pRFrVvJvn+5uR3OPkRe09MOpSJgckXVjmwWyq5zsofH2P52Rtm2WUY23mQYovmzE0g4IloVR89XVHcdQxhwszvdp7zmisqpXF0bJnRxjoEcbi1oA6MQOI4fSJO8r1ncommdo0q5d8rZftsDIa2ptNPXVjgAHy1tbCyoqHyO3vLHPELC7a2GKNgwawAYF1SiyTSiJREoiURKIp4ft7c/3NmPg48sm8Zh1NS2BMkSmOYJy+XO6ep4+dwUDc1pMlnKpzLqIxDmVkI9qQ34UGLNBImhEylLlVpDdp7jlc0tS4ukpJjG0nae7LWuYMfMS5o8jQANgXm88T/TO0ZE5iGZgsUDKeizLaY66ZrAGsNa2aanqXho2AytjhmkI2vmlke7tOJPu1V1FrhSiLVCXhMqXHd103CsberPXHNzKp9d25SUk3T45t3+W4y4jr8awHqZDNUSTHe97j6ySvaZYqBlqslHa2DBlNSxRAeQRxtYPYqdqCpqu8tdFFzc1ut3CSa6C87EIroqkKoksirINyKJKJmASnTUIYQEBDQQGosADp2NO0F49ql14kfFaKqWIlsraaUgg4EEMcQQegg7QVMa5z+3cxnkT7xkLhdOM8R3ir33zjEF0un7zF824Hcsola82JZCesN04UE2xBQJCM3CRNMjFEomDIzNOj1DWcVZlp4p6nf3LiTE781210Z8x4m9ADQtEPLp4pWbsrdxlfX6nkvliGDG3OnaxtwibuBqIuxDWNaMMXtMNRgHPe6pkOBiZ54455v4x3w7xznjG1zY2uxt3ToM55mAMJhokp2hk7bnWh3UFc8OZT8JXce5cthMAl37gEAsDdbNdLHVGjusD4agdDhsI8rXDFrh52kjzrdfpvqpp5q9l1matN7tSXayvwBdC7txOIx7ueFwbNTy4bTFMyN+G3hwIKsrUsVwEoil4+19mVF7R5k28JtUou48HzJCa/2qT0ZlFiqbb8NxbcIGvx0+VZC6HSE09yh6GvgP2hKPurR14wVAyO95CugHbmpbtET5oZLe8ervz61Ksq/K0xpRFqaHKCjVwu1VDRVssqgqHXooicyZw6gA9DFGsAnAtJad4K9r0UjZomzM9x7QR6CMQvjXC+1UNo/wDbLY/9FC/8k2qNT/zhn57faFK75/klZ/VZf4Ny2omRsmY8xBZ8vkDKV62zj+yoFHvy1z3bMMoOHZlHXtJGeP1kU1Hbk4bEECbll1BAiZTHECjndWVtHbqd1XXSshpmDa55DQOs9J6BvO4LxqZVyjmjPN9gyxk231dzzBUuwip6aJ80rvKeFgJDWja95wYxuLnENBKiJ+VTzaYQ5E2RcnHfA+FbVyrabwXLVfNGZbXFVOMdiQ6AT2H7RdlazsHMppm1bTUgozcpAJyfQCUwKVj3nzU213ilfZ7VTR1FOcR30zdx+tCw4OafI92BH1OlbwuTLw+dQ9Lcw0mqWpGYK2y3qPhcLVa6jAyNxB7m51LeKGaIn36WFsrHdl34kEFqjDVY9bfUoilqe11QUK35wOhD9pZbjcgQevVRsTPKioemnQron9ayA0MBwujugmm+Tv8A9laS/GHkaZdPIR77W30n0ONnA+EqWTV/lpSSiLVhchrRcWBn7ONiO0DNnVl5fyXabhuYokMgtbt5zUQokJTAAl2HZiHpWCV4pzSXaqpXDB0VRIz7L3D5l7J9Lr3FmbTPLuY4XccNwsVBUtdvxE9LFKD1hys/UuVdLmRz5aMkGMk3BMy8e8avkCqgYyRlmi5F0wUKUxDGTE6YagAgIh8Qr6Y4seHjeCD6lAqqdlXSyUkuIiljcw4b8HAg4b9uB2bCslOUPMrkfzGvAbxz/kuavFVsuupAWymcIqx7RRW1KLW1bQYdmFiQ7IFTUcAmd66KQouF1jhvGdXzMl5zHUfibtO6Qg9lu5jPM1g7I9OGJ6SSrTaPaC6U6EWL/AdM7RT0DHtAmqCO8q6kj6VRUvxlk24uDOIRRkkRRxt7KxfqRq8CURKIpl/tj7RcMsA8mL7OgYrW5Mv2vaaDgSiBVlrKswku5SKYQ0N2CX6mI6D07lZIaI05baa2qw7L6hrPsMx/jAtCvi8XuKo1MyjlxrsZqSxVFSW+QVdV3TT1mjd6lJuq9y1EJRFFX8sPg7y1mzNd1cmOIpLduCSyQ5JMZGxFNTkbakkF5CgkhIXRZs5OLMradNLlMj9S/av3bNRF+ZVVJRYi/abWGz9pfcLnc5L3l7ge+Y4yQlwYePpcxzsGkO3uDiCHYkEg4N3NclfiIZJ0+0+otJNcDVUtJaWGKhucUL6mP8LiSynqoYQ+droMe7hkhila+EMY9kbo+OXwhn/Eh5IbbVURkeI+T3J0hEDDAFt260h2joPbXtedmEFQ/hsMbX4VauXT7OcJwfb5yfyeF/wuK2P2znd5UbswSUud7Oxp/Td/THrbUQxEdYCts+8c3PmPOYjjhnyZUEogAixwvf0mQdf9VI2DdpnD+QjXSdk3NjDgbbW9UMh9jSqspuanlpqmh0WfcogH691o4z6pJmkLpR4Cc6yiJR4Wcs9QEQHTjnmAwagOnQxbOEpg+YDoNQ/+p5q/0y4fq837RTAcy/LiRiNQMk/75bP70uQh4/Od7kQBPhdyrKIm2/v8fsrNQ1016i6tREALp8fSuRlHNTt1tr/1eUe1ihScznLfEMXZ/wAlnZ0Xq3O+GpKq+L8YnkJl9n0nDvPaW/0+6WBMQmn5/vSTDt/q0rsR5HzfJ7tuq+uMt+LBSKs5vuV+hx77PeWnYfo62KX1d0X49SyPxX4L/JFkuajmMnhNni2DduEUnt2ZKvizYyOiUVDBvcOYOEmp+9HIJk1Ha3jFR16DprU6oNLc51srWvpRBETtfI9gA9LWuc/1NKtTnPxF+VDKVBLUUmYJLzcWNJZTUFJVSPkI3NbNNFDStxPS+oaOnaptHBriHZ3B/jhZOAbQkVbgVhTPpy8LucNCsHN43xPKEcT9wHYkVXKxamFJJqyb9xU7dg1QTOoqchlT5NZWy9TZXs0Vpp3cZbi578MON7vedh0DcANuDQASTtXnz5itcb9zD6rXDUy+RClZUcENLTNdxtpaSEFsMIeQ3jdtdJK/haHzSSPaxjXBjcu6qFWOSiJREoiURKIlESiJREoiURKIlESiJREoiURKIlESiJREoiURKIlESiJREoiURKIlESiJREoiURKIlESiJREoiURf/9k=';
					} else if (object.type == 'content') {
						objectGallerySrc =
							'data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAABkAAD/4QMvaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjMtYzAxMSA2Ni4xNDU2NjEsIDIwMTIvMDIvMDYtMTQ6NTY6MjcgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkJFNTc5NUM3RTk5QzExRThBQjQzRDZGNEYwNThFNEQyIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkJFNTc5NUM2RTk5QzExRThBQjQzRDZGNEYwNThFNEQyIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDQyAyMDE4IChXaW5kb3dzKSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjQ1ODUxOUEzQ0VGMTExRTg4Qzk1Qzg5OURDRTk4NjNEIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjQ1ODUxOUE0Q0VGMTExRTg4Qzk1Qzg5OURDRTk4NjNEIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+/+4ADkFkb2JlAGTAAAAAAf/bAIQAAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQICAgICAgICAgICAwMDAwMDAwMDAwEBAQEBAQECAQECAgIBAgIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMD/8AAEQgAZABLAwERAAIRAQMRAf/EAIUAAQEBAQEAAAAAAAAAAAAAAAAJCggHAQEBAQAAAAAAAAAAAAAAAAAACAcQAAAEBAEICAUFAQAAAAAAAAAFBgcCAwQIARMUFpYX1wkaEhVXpxhYqBlWaClpCiMk2OiZJhEBAAECBQQCAwEAAAAAAAAAAAECAxEEBQYHEqTUVRQXIRMYCP/aAAwDAQACEQMRAD8A38AAAAAAAAAAAAAAAAAAAAAAAAAAAAzbcYi/q7S1i5lDN8wzsaCJA4YpMrIyKNBG0U+cqQwX7nElYZZ+skaoTSTlitPUcrIy58MiHI9KGDCOOOKKtuBeMdj702fmdU3Nkfk5+3qVy1TV+7MW8LdNjL1xT02rtFM4VV1TjMTV+cJnCIiIE/1RzXybxvyFk9D2XqfwtLu6Nav10fHyl7G7Vmc3bqr6r9i7XGNFqiOmKopjpxiMZmZk/wC8hxIPMb3QsRuvG3fQXEvqe6znkJo/qznz33Zad4h7yHEg8xvdCxG68PoLiX1PdZzyD+rOfPfdlp3iHvIcSDzG90LEbrw+guJfU91nPIP6s58992WneIe8hxIPMb3QsRuvD6C4l9T3Wc8g/qznz33Zad4jSVwd7lnsuntmXLgvytdO1eTvqpkaWm+jiSTGbJsvQDYndGW5gjSFPFc7ImihrJuWmSIp8WW6MUeMEEEMMkc9bQ27sveGW0vbOX+NkLmm27tVP7LtzG5VfzFE1dV2uuqMaaKYwiYp/GMRjMzN9/5X5A3dyRx7nNc3pm/m6pa1m7Yor/VZs4WqctlLlNHTYt2qJwru1z1TTNU9WEzhERFXxiSlwAAAAAAAAAAAAAAAABlC463uY+Ldu/Bl46tl/hzSPX3hi2/6Bae7THd60632Vf8APaX6PdV5xlv3uZZr0/0siAi19en7u3rKAPr0/d29ZQB9en7u3rKAPr0/d29ZQB9en7u3rKAdGWee9Z4t7Wtqvuj7L/EYyO0jaD4sdAtAtpiY0w030j/57RDR7OOss/8A2WZZTL/pdIBvWAAABC/idcaT24n2R7KeGvbJpY0pK6Wku2PZ5mHXCxXKT6i6m2VrnOs30LzjOs7l9POehkocn04wnHzU3yI+p/8ArwAc1N8iPqf/AK8AHNTfIj6n/wCvABzU3yI+p/8ArwAuvwyb+/cZYNUvhsn2OaNO0ftboxp3tCz3qNIoVVde9daGofNs601yGa5pM6GbdPKxZToQBRUAAAABKO+u3fhIu47KbUV/CgYspd2gbsrJUzTOddCdMkfRtxIUqsriqfRpYtdtAyTEoiU5gbwyzDGjmxTZ0M2TlosJGEEsOKvBH+Nj8aWif6Bqr+SYB4I/xsfjS0T/AEDVX8kwDwR/jY/Glon+gaq/kmAeCP8AGx+NLRP9A1V/JMB3ZbI6XCLs4QJo2FuFz1ojcoY6VpgujMj8XqPV+cqo0KCIhrjTrNduuqDiTlypNUUrIS6iCmhyHShl4RxzIog7/bF4Wkewgq1WzLpNy7iXoDeoT9cpGxW6ZXpBRH1JR0BjVElWcJUzNS6nN6YvNaWfMpo5mE6CTUyo8YcIZkGOIejAAAAzc8Yjg83McQe5VCPIzK5YtMphMManGyr6BzlMvyU+nHxQvnKVNTV0dKlWyWhfMKIy9ZUsEuZHVS52M6XNwxlQwwwRxhJ7ljb9O1u0TX15dwYByxt+na3aJr68u4MA5Y2/TtbtE19eXcGAcsbfp2t2ia+vLuDAOWNv07W7RNfXl3BgNJfB6sWdvh9WyrVmHmUTcqZUKR8lO5tDXtibqY5IJRCdINtEvS0lXVKpIosxgN5ZgjaqOZLgpZknCTMlY4TYooo4IAq4AAADKTx2OJRetZtda2ra23PRs4RSgt6TS5NyXZy06vztUmDkOmQVZp1ivEIqDaRlilOUUrISp8FPDkelDLwjjmRRBFP33+K15qO4+3DdAAe+/wAVrzUdx9uG6AA99/iteajuPtw3QAHvv8VrzUdx9uG6ABrF4F92L/3jWiLp0Lj19tGXRNcKsEMWnmiyKSObJYrb1qzygK+rEInEwTzsgaKStm5eZTxVEWW6MUzGCCXDCFnQAAAAEoL6+MPbNw+XcTrMvMhn1UyoUzclDnUFe2KZQByQSSA5UyvStLSVdUqnORZjLN5Zii6qOZLgpZknCTMlY4TYooo4IA4t5nKwvsku71CZrf4AczlYX2SXd6hM1v8AADmcrC+yS7vUJmt/gBzOVhfZJd3qEzW/wA5nKwvsku71CZrf4A9GZ78iSyl7Xca1mUq190heqHccZENim69QIppqQgoj9eqYsSpPVndUWvabGNMUUxibS46mZIpamdBJhixglTIsMIMQvWAAACUF9fB4tm4gzuJ15nmXL6plUJluShsaCgbFTIAmIJxATKZXqqlq6ulVTYrQxmG8wxWlVBMmQVUuTjJlysMJUMUMccYcW8sbYX2t3d6+s1uDAOWNsL7W7u9fWa3BgHLG2F9rd3evrNbgwDljbC+1u7vX1mtwYByxthfa3d3r6zW4MB6Mz347dlLJO41rzJV0LpDBUNG4yIc5N0CgWrTVZBWn6CUxYqiekO6UtZIpMakoqTEplwVMuRVU06OTFFhBNlxY4R4BesAAAAAAAAAAAAAAAAAAAAAAAAAAAAB//9k=';
					} else if (object.type == 'link') {
						objectGallerySrc =
							'data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAABkAAD/4QMvaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjYtYzE0MiA3OS4xNjA5MjQsIDIwMTcvMDcvMTMtMDE6MDY6MzkgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDQyAyMDE4IChXaW5kb3dzKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpBRjU1RDZCMENFRjExMUU4QTVFQTk4NUI3MjJCNjM0NSIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDpBRjU1RDZCMUNFRjExMUU4QTVFQTk4NUI3MjJCNjM0NSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOkFGNTVENkFFQ0VGMTExRThBNUVBOTg1QjcyMkI2MzQ1IiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOkFGNTVENkFGQ0VGMTExRThBNUVBOTg1QjcyMkI2MzQ1Ii8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+/+4ADkFkb2JlAGTAAAAAAf/bAIQAAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQICAgICAgICAgICAwMDAwMDAwMDAwEBAQEBAQECAQECAgIBAgIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMD/8AAEQgAZABLAwERAAIRAQMRAf/EAHAAAQADAQEAAAAAAAAAAAAAAAAICQoHCwEBAAAAAAAAAAAAAAAAAAAAABAAAAUEAAMHAQUJAAAAAAAAAAMEBQYBAgcIFBcJERITFRYYCiMhMphZGjU2J9eo2OgZaREBAAAAAAAAAAAAAAAAAAAAAP/aAAwDAQACEQMRAD8A38AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMfXyE+q9twy7I4X6OvTIUO7PtjsKnjN2RMmxhWU2yyGNk+PVkxWCwuQGXUpBXVUwoVD9IZJ9E1iYapzkp5V955yYIXsfwr5rklvTzHaLqcyiT5geiilkpUMmHHWfoEjmfZQxelJnmQsvN8nlxdFd11bVqhua7zaV7bk9ta1AX79E3odRboye5j01sTIM++5DkzxvnuOG7H3pPk/zW4bheAmEs83895pGd/v8AD8PwdvZ3/Er3Aog/Q6Yt/MTn/wCHCO/zhARI2F096rPxanbHu2+vO2zptlpWfN2eIZYxs/N8oisNK84PvubWLI2JF8snzFG0skJJvQtMyY19i5C6VsJNsT2qiU60N92pmy2Ptx9acJbR4rvV0gecceR6fsSNy8Gjqy1d0lKukaeqJrzU1r5FnopS2rqFX3lUVpTKWXXW9l1QkMAAAAA8/fqLTtt6ZfyzcEb3bHol7drdnKLRQ9uyCchUq2WLNLprap1JlzqnVElH9hmNJKWne3tKntMWFM7haZaVWqsjvhrj6iupkO6tvTtyzq/j/PsaiEN2LQ4rdI9niHsrXmuNENsDy/j3LqZwZ2pmnERa5WikBcGohsNJeiLCaK+IpU3w/BMDJR+hj/6if0Tf5cAJ/wDS4+KV/rX3swZur78+dHJfmb/DT2ucuvUvMXDuQcT/AL5e4qd+T+T+u/MP2Uq4jhfA+l4vjFh0f5Z+9uveJemnlLTtwmMYkWxGzTxjFmjGMG50b3OTRSLwrJ8Nyi/ZHlrMQcYrj7AUnhVrc3GqaEmLHNcXVPacWmV1JCz3oKYByDrJ0hNHMP5TbHJjnjbi96mr6wvJJyV5j3N/I02zC1x14QqKWqW11YGaeJ0alKbbYalOIuJvttusrbQLeQAAAAEG9/8Ap16sdS/BivAu1EGMkcfKWXvUNl7AsLY8i4ylVU16QuV4/lFUq2rS6UIv7h5CghW2ryqUKWJVBVKWAMmjr8KApndnKmHeqfk3HMSWKr1BDC665WyN2tp9wirk+RbYrGrY5qiyKUtqba1kd7s+y22n2AL0eib0XZT0hPcx6l3HkG2vuF5M8F57itxxn6A5Tc1uJ4Xj8y5b879VczC+/wBzy/hvLbe3x/Fp4IUQfouspfnHz/8ACxIv7yQFhPTw+JfpVptl2PZ7ztk+ZbpZNhjsmkUNbpzEmiB4kaZSkU2LkcsdMdpnuaOUtfW1cXQ1LY6PaltLN+oYjNOtKMKDVoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP//Z';
					}
				}

				$('#mhLightbox_wrapper_gallery').append("<img src='" + objectGallerySrc + "' class='mhLightbox_gallery_object' mhlightboxgallerygroup='" + group + "' mhlightboxgallerygroupind='" + index + "'>");
			});
		}
		function init() {
			objects = {};
			objectInstance = $(_settings.selector);
			objectCount = objectInstance.length;
			if (objectCount > 0) {
				var objectGroupKey = 0;
				objectInstance.each(function(index, element) {
					var object = $(element);
					var objectHref = object.attr('href');
					objectHref =
						typeof objectHref !== 'undefined' && objectHref !== ''
							? objectHref
							: '';
					if (objectHref !== '') {
						var objectGroup = object.attr('mhlightbox');
						objectGroup =
							typeof objectGroup !== 'undefined' &&
							objectGroup !== ''
								? objectGroup
								: objectGroupDefault;
						var objectTitle = object.attr('title');
						objectTitle =
							typeof objectTitle !== 'undefined' &&
							objectTitle !== ''
								? objectTitle
								: '';
						var objectType = '';
						// get type of object
						var objectImgData = null;
						if (objectHref.match(imageTypesRegex) !== null) {
							// Image
							objectType = 'image';
							objectImgData = {};
							var objectImg = object.find('img')[0];
							if (typeof objectImg !== 'undefined') {
								var objectImgSrc = objectImg.src;
								objectImgData.src =
									typeof objectImgSrc !== 'undefined' &&
									objectImgSrc !== ''
										? objectImgSrc
										: '';
								var objectImgAlt = objectImg.alt;
								objectImgData.alt =
									typeof objectImgAlt !== 'undefined' &&
									objectImgAlt !== ''
										? objectImgAlt
										: '';
								var objectImgTitle = objectImg.title;
								objectImgData.title =
									typeof objectImgTitle !== 'undefined' &&
									objectImgTitle !== ''
										? objectImgTitle
										: objectTitle;
								objectImgData.maxWidth =
									_settings.imageMaxWidth;
								objectImgData.maxHeight =
									_settings.imageMaxHeight;
							}
						} else if (objectHref.match(/www\.youtube/) !== null) {
							// youtube video
							objectType = 'video';
							var objectVideoId = objectHref.slice(
								objectHref.length - 11
							);
							objectHref =
								'https://www.youtube-nocookie.com/embed/' +
								objectVideoId +
								'?rel=0&showinfo=0';
						} else if (objectHref.substr(0, 1) == '#') {
							// content
							objectType = 'content';
							// uses default width and height
						} else if (objectHref.match(/https?:\/\//) !== null) {
							objectType = 'link';
						}

						objectGroupNegate = false;
						if (objectGroup.charAt(0) === '!') {
							objectGroup = objectGroup.substring(
								1,
								objectGroup.length
							);
							objectGroupNegate = true;
						}

						// next group = reset
						if (!(objectGroup in objects)) {
							objects[objectGroup] = [];
							objectGroupKey = 0;
						} else {
							objectGroupKey = objects[objectGroup].length;
						}
						if (!objectGroupNegate) {
							// object data extracted
							var objectData = {
								active: false,
								href: objectHref,
								title: objectTitle,
								type: objectType,
								img: objectImgData,
							};
							objects[objectGroup][objectGroupKey] = objectData;
							objects[objectGroup].active = false;
							object.attr('mhlightboxind', objectGroupKey);
						}
						object.attr('mhlightbox', objectGroup);
						objectGroupKey++;
					}
				});
			}

			_settings.initCallback(objects);
		}
		function destroy() {
			$(document).off('click', _settings.selector);
			$(document).off('touchstart, touchmove, touchend');
			$(document).off('keyup');
			$(document).off(
				'click',
				'#mhLightbox_prev, #mhLightbox_next, #mhLightbox_close, #mhLightbox_container, #mhLightbox_wrapper, #mhLightbox_object_subtitle a, .mhLightbox_gallery_object, .mhLightbox_img_edit_rotate, .mhLightbox_img_edit_flip, .mhLightbox_zoom_icon_plus, .mhLightbox_zoom_icon_minus'
			);
		}
		function toggleControls(curKey, maxKey) {
			// prev, next, gallery
			curKey = parseInt(curKey);
			maxKey = parseInt(maxKey);
			$('#mhLightbox_next, #mhLightbox_prev').removeClass('disabled');
			if (curKey + 1 == 1) {
				$('#mhLightbox_prev').addClass('disabled');
			}
			if (curKey + 1 == maxKey) {
				$('#mhLightbox_next').addClass('disabled');
			}
			if (maxKey <= 1) {
				$('#mhLightbox_wrapper_gallery').hide();
				$('#mhLightbox_wrapper').addClass('single');
			} else {
				$('#mhLightbox_wrapper_gallery').show();
				$('#mhLightbox_wrapper').removeClass('single');
			}
			// img edit
			imgFlip = 0;
			imgEdits.rotation = 0;
			imgEdits.flip = 1;
			// zoom
			zoomLevel = 1;
			imgEdits.zoomLevel = zoomLevel;
			imgEdits.mouseDragX = 0;
			imgEdits.mouseDragY = 0;
			if(objectActive.type=='image') {
				$('#mhLightbox_zoom_icon_plus').removeClass('disabled');
				$('#mhLightbox_zoom_icon_minus').addClass('disabled');
				$('#mhLightbox_zoom').removeClass('hidden');
				$('#mhLightbox_img_edit').removeClass('hidden');
			} else {
				$('#mhLightbox_zoom').addClass('hidden');
				$('#mhLightbox_img_edit').addClass('hidden');
			}

			dragEnd(true);
			// Peek Controls if hidden
			if ($('#mhLightbox_panel').is(':hidden')) {
				$('#mhLightbox_panel').fadeIn('fast').delay(3000).fadeOut('fast');
			}
			if(objectActive.type=='image') {
				if ($('#mhLightbox_img_edit').is(':hidden') && _settings.imageEditActions) {
					$('#mhLightbox_img_edit').fadeIn('fast').delay(3000).fadeOut('fast');
				}
				if ($('#mhLightbox_zoom').is(':hidden') && _settings.imageEditActions) {
					$('#mhLightbox_zoom').fadeIn('fast').delay(3000).fadeOut('fast');
				}
			}
		}
		function resize(object, callback) {
			var galleryObjectHeight = $('#mhLightbox_wrapper_gallery').is(':visible') ? $('#mhLightbox_wrapper_gallery').outerHeight() : 0;
			var wrapperMinWidth = parseInt($('#mhLightbox_wrapper').css('min-width'));
			var wrapperMinHeight = parseInt($('#mhLightbox_wrapper').css('min-height'));
			var wrapperPaddingWidth = 0;
			var wrapperPaddingHeight = 0;
			var lbPaddingInt = parseInt(lbPadding);
			var lbPaddingType = lbPadding.replace(lbPaddingInt, '');
			if (lbPaddingType == '%') {
				wrapperPaddingWidth = (browserWidth * lbPaddingInt) / 100;
				wrapperPaddingHeight = (browserHeight * lbPaddingInt) / 100 + galleryObjectHeight;
			} else {
				wrapperPaddingWidth = lbPaddingInt;
				wrapperPaddingHeight = lbPaddingInt + galleryObjectHeight;
			}
			var objectWidth = 0;
			var objectHeight = 0;
			$('#mhLightbox_wrapper').css('max-width', 'auto');
			$('#mhLightbox_wrapper').css('max-height', 'auto');
			if (object.type == 'image') {
				if (object.title !== '' && typeof object.title !== 'undefined') {
					$('#mhLightbox_object_subtitle').show();
				} else {
					$('#mhLightbox_object_subtitle').hide();
				}
				objectWidth = object.naturalWidth;
				objectHeight = object.naturalHeight;
				var objectRatio = objectWidth / objectHeight;
				if (objectWidth >= browserWidth - wrapperPaddingWidth) {
					objectWidth = browserWidth - wrapperPaddingWidth;
					objectHeight = objectWidth / objectRatio;
				}
				if (objectHeight >= browserHeight - wrapperPaddingHeight) {
					objectHeight = browserHeight - wrapperPaddingHeight;
					objectWidth = objectHeight * objectRatio;
				}
				if (objectWidth <= wrapperMinWidth) {
					objectWidth = wrapperMinWidth;
					objectHeight = objectWidth / objectRatio;
				}
				if (objectHeight <= wrapperMinHeight) {
					objectHeight = wrapperMinHeight;
					objectWidth = objectHeight * objectRatio;
				}
				objectWidth = objectWidth + 'px';
				objectHeight = objectHeight + 'px';
			} else {
				objectWidth = browserWidth - wrapperPaddingWidth + 'px';
				objectHeight = browserHeight - wrapperPaddingHeight + 'px';
				$('#mhLightbox_object_subtitle').hide();
				if (object.type == 'video') {
					objectWidth = '90vw'; // 75% of browser width
					objectHeight = 'calc(90vw * 0.5625)'; // 16:9 = 56.25%
				} else if (object.type == 'content') {
					objectWidth = '800px';
					objectHeight = '600px';
				}
			}
			$('#mhLightbox_wrapper').css('width', objectWidth);
			$('#mhLightbox_wrapper').css('height', objectHeight);

			if (typeof callback === 'function') callback();
		}
		function move(direction) {
			direction = typeof direction !== 'undefined' ? direction : null;
			if (direction !== null) {
				var min = 0;
				var max = 0;
				var thisObjectGroupKey = 0;
				for (var objectGroup in objects) {
					if (objects[objectGroup].active) {
						for (var objectGroupKey in objects[objectGroup]) {
							if (objects[objectGroup][objectGroupKey].active) {
								thisObjectGroupKey = parseInt(objectGroupKey);
								max = parseInt(objects[objectGroup].length) - 1;
							}
						}
					}
				}
				var thisGalleryObjectGroupKey = 0;
				if (direction == 'previous') {
					if (thisObjectGroupKey === 0) {
						thisGalleryObjectGroupKey = max;
					} else {
						thisGalleryObjectGroupKey = thisObjectGroupKey - 1;
					}
				} else if (direction == 'next') {
					if (thisObjectGroupKey == max) {
						thisGalleryObjectGroupKey = min;
					} else {
						thisGalleryObjectGroupKey = thisObjectGroupKey + 1;
					}
				}
				$(
					'.mhLightbox_gallery_object[mhlightboxgallerygroupind="' +
						thisGalleryObjectGroupKey +
						'"]'
				).trigger('click');
				$('#mhLightbox_object_count').html(
					thisGalleryObjectGroupKey + 1 + ' / ' + (max + 1)
				);
				toggleControls(thisGalleryObjectGroupKey, max + 1);

				_settings.moveCallback(direction);
			}
		}
		function imgEdit() {
			var transformString = '';
			$.each(imgEdits, function(idx, val) {
				switch (idx) {
					case 'rotation':
						transformString += 'rotate('+val+'deg) ';
						if(val==90 || val==270) {
							var objW = $('#mhLightbox_object').width();
							var objH = $('#mhLightbox_object').height();
							if(objH>$('#mhLightbox_wrapper').width()) {
								$('#mhLightbox_object').css('height', objW+'px');
								$('#mhLightbox_object').css('width', ($('#mhLightbox_wrapper').width()*objW/objH)+'px');
							} else if(objW>$('#mhLightbox_wrapper').height()) {
								$('#mhLightbox_object').css('width', objH+'px');
								$('#mhLightbox_object').css('height', ($('#mhLightbox_wrapper').height()*objH/objW)+'px');
							}
						} else {
							$('#mhLightbox_object').css('height', '');
							$('#mhLightbox_object').css('width', '');
						}
						break;
					case 'flip':
						transformString += 'scaleX('+val+') ';
						break;
					case 'zoomLevel':
						transformString += 'scale('+val+') ';
						break;
					case 'mouseDragX':
						transformString += 'translateX('+val+'px) ';
						break;
					case 'mouseDragY':
						transformString += 'translateY('+val+'px) ';
						break;
				}
			});
			$('#mhLightbox_object').css('transform', transformString);
		}
		function rotate() {
			imgEdits.rotation += 90;
			if(imgEdits.rotation==360) imgEdits.rotation = 0;
			imgEdit();
		}
		var imgFlip = 0;
		function flip() {
			imgEdits.flip = (!imgFlip ? -1 : 1);
			imgEdit();
			imgFlip = !imgFlip;
		}
		var zoomLevel = 1;
		function zoom(direction) {
			direction = typeof direction !== 'undefined' ? direction : null;
			if (direction !== null) {
				var min = 1;
				var max = 10;
				var step = 0.5;
				if (direction == 'plus') {
					var levelNext = zoomLevel + step;
					if(levelNext>=min && levelNext<=max) {
						imgEdits.zoomLevel = levelNext;
						imgEdits.mouseDragX = mouseDragEnd.x;
						imgEdits.mouseDragY = mouseDragEnd.y;
						imgEdit();
						zoomLevel = levelNext;
						if(zoomLevel<max) {
							$('#mhLightbox_zoom_icon_plus').removeClass('disabled');
						} else {
							$('#mhLightbox_zoom_icon_plus').addClass('disabled');
						}
						if(zoomLevel>min) {
							$('#mhLightbox_object').addClass('dragable');
							$('#mhLightbox_zoom_icon_minus').removeClass('disabled');
						} else {
							$('#mhLightbox_object').removeClass('dragable');
							$('#mhLightbox_zoom_icon_minus').addClass('disabled');
						}
					}
				} else if (direction == 'minus') {
					var levelNext = zoomLevel - step;
					if(levelNext>=min && levelNext<=max) {
						dragEnd(true);
						imgEdits.zoomLevel = levelNext;
						imgEdits.mouseDragX = mouseDragEnd.x;
						imgEdits.mouseDragY = mouseDragEnd.y;
						imgEdit();
						zoomLevel = levelNext;
						if(zoomLevel>min) {
							$('#mhLightbox_object').addClass('dragable');
							$('#mhLightbox_zoom_icon_minus').removeClass('disabled');
						} else {
							$('#mhLightbox_object').removeClass('dragable');
							$('#mhLightbox_zoom_icon_minus').addClass('disabled');
						}
						if(zoomLevel<max) {
							$('#mhLightbox_zoom_icon_plus').removeClass('disabled');
						} else {
							$('#mhLightbox_zoom_icon_plus').addClass('disabled');
						}
					}
				}
			}
		}
		var dragActive = false;
		var dragStartMouse = { x: 0, y: 0};
		var mouseDrag = { x: 0, y: 0 }
		var mouseDragEnd = { x: 0, y: 0 }
		function dragInit(e = null) {
			if(e!=null) {
				dragActive = true;
				dragStartMouse = { x: e.clientX, y: e.clientY };
				if(mouseDragEnd.x!=0) {
					dragStartMouse.x = dragStartMouse.x - mouseDragEnd.x;
				}
				if(mouseDragEnd.y!=0) {
					dragStartMouse.y = dragStartMouse.y - mouseDragEnd.y;
				}
			}
		}
		function dragEnd(empty = false) {
			dragActive = 0;
			dragStartMouse = { x: 0, y: 0 };
			if(!empty) {
				mouseDragEnd = mouseDrag;
			} else {
				mouseDragEnd = dragStartMouse;
			}
		}
		function drag(e = null, el = null) {
			if(e!=null && el!=null && dragActive) {
				var objectWidth = el.width();
				var objectHeight = el.height();
				var imgWidth = objectWidth * zoomLevel;
				var imgHeight = objectHeight * zoomLevel;
				var imgDragLeftRight = (imgWidth - objectWidth) / (2 * zoomLevel);
				var imgDragTopBottom = (imgHeight - objectHeight) / (2 * zoomLevel);
				if(imgEdits.rotation==90 || imgEdits.rotation==270) {
					var imgDragLeftRightTemp = imgDragLeftRight;
					imgDragLeftRight = imgDragTopBottom;
					imgDragTopBottom = imgDragLeftRightTemp;
				}
				
				var mouseDragX = (e.clientX - dragStartMouse.x);
				var mouseDragY = (e.clientY - dragStartMouse.y);

				var updateObject = true;
				if(mouseDragX>=(-1*imgDragLeftRight) && mouseDragX<=imgDragLeftRight) {
					mouseDrag.x = mouseDragX;
				} else {
					updateObject = false;
					if(mouseDragX>=0) {
						dragStartMouse.x = e.clientX - imgDragLeftRight;
					} else {
						dragStartMouse.x = e.clientX + imgDragLeftRight;
					}
				}
				if(mouseDragY>=(-1*imgDragTopBottom) && mouseDragY<=imgDragTopBottom) {
					mouseDrag.y = mouseDragY;
				} else {
					updateObject = false;
					if(mouseDragY>=0) {
						dragStartMouse.y = e.clientY - imgDragTopBottom;
					} else {
						dragStartMouse.y = e.clientY + imgDragTopBottom;
					}
				}
				if(updateObject) {
					imgEdits.zoomLevel = zoomLevel;
					if(imgEdits.rotation==90) {
						imgEdits.mouseDragX = (imgEdits.flip==-1 ? mouseDrag.y * -1 : mouseDrag.y);
						imgEdits.mouseDragY = mouseDrag.x * -1;
					} else if(imgEdits.rotation==180) {
						imgEdits.mouseDragX = (imgEdits.flip==-1 ? mouseDrag.x : mouseDrag.x * -1);
						imgEdits.mouseDragY = mouseDrag.y * -1;
					} else if(imgEdits.rotation==270) {
						imgEdits.mouseDragX = mouseDrag.y * -1;
						imgEdits.mouseDragY = (imgEdits.flip==-1 ? mouseDrag.x * -1 : mouseDrag.x);
					} else {
						imgEdits.mouseDragX = mouseDrag.x;
						imgEdits.mouseDragY = mouseDrag.y;
					}
					imgEdit();
				}
			}
		}
		function animate(object, firstcall) {
			firstcall = typeof firstcall !== 'undefined' ? firstcall : false;
			var timeout1 = 150;
			var timeout2 = 300;
			if (firstcall) {
				timeout1 = 10;
				timeout2 = 10;
			}
			$('#mhLightbox_object').fadeOut(timeout1, function() {
				$('#mhLightbox_object').remove();
				$('#mhLightbox_wrapper').prepend(
					"<img id='mhLightbox_object' style='display:none'>"
				);
				resize(object, function() {
					setTimeout(function() {
						$('#mhLightbox_object').attr('src', object.src).fadeIn(timeout2);
					}, timeout2);
				});
			});
		}

		// bind events
		function bind() {
			$(document).on('click', _settings.selector, function(e) {
				e.preventDefault();
				if ($.isEmptyObject(objects)) {
					init();
				}
				var objectClicked = $(this);
				var objectGroup = objectClicked.attr('mhlightbox');
				objectGroup =
					typeof objectGroup !== 'undefined' && objectGroup !== ''
						? objectGroup
						: objectGroupDefault;
				var objectGroupKey = objectClicked.attr('mhlightboxind');
				objectGroupKey =
					typeof objectGroupKey !== 'undefined' &&
					objectGroupKey !== ''
						? objectGroupKey
						: 0;
				if (objects[objectGroup].length > 1) {
					createGallery(objectGroup, objectGroupKey);
				}
				if (
					!$.isEmptyObject(objects[objectGroup]) &&
					!$.isEmptyObject(objects[objectGroup][objectGroupKey])
				) {
					var object = objects[objectGroup][objectGroupKey];
					objects[objectGroup].active = true;
					object.active = true;
					$('.mhLightbox_gallery_object[mhlightboxgallerygroup="' + objectGroup + '"][mhlightboxgallerygroupind="' + objectGroupKey + '"]').addClass('active');

					if (object.type == 'image') {
						var objectActiveTitle =
							object.img.title !== '' &&
							typeof object.img.title !== 'undefined'
								? object.img.title
								: object.title !== '' &&
								  typeof object.title !== 'undefined'
								? object.title
								: object.alt !== '' &&
								  typeof object.alt !== 'undefined'
								? object.alt
								: '';
						objectActive = new Image();
						objectActive.src = object.href;
						objectActive.title = objectActiveTitle;
						objectActive.type = object.type;
						$('#mhLightbox_object_count').html(parseInt(objectGroupKey) + 1 + ' / ' + objects[objectGroup].length);
						$('#mhLightbox_object_subtitle').html(object.img.title).show();
						objectActive.onload = function() {
							animate(objectActive, true);
							$('#mhLightbox_container').fadeIn(500);
						};
					} else {
						objectActive = {};
						objectActive.type = object.type;
						$('#mhLightbox_object').remove();
						$('#mhLightbox_wrapper').prepend("<iframe id='mhLightbox_object' frameborder='0'></iframe>");
						$('#mhLightbox_object_count').html(parseInt(objectGroupKey) + 1 + ' / ' + objects[objectGroup].length);
						resize({ type: object.type }, function() {
							if (object.type == 'content') {
								var iframe = document.getElementById('mhLightbox_object');
								iframe =
									iframe.contentWindow ||
									iframe.contentDocument.document ||
									iframe.contentDocument;
								iframe.document.open();
								iframe.document.write(
									$('' + object.href).html()
								);
								if (_settings.inlineCSS !== '')
									$('#mhLightbox_object')
										.contents()
										.find('head')
										.append(
											'<link rel="stylesheet" href="' +
												_settings.inlineCSS +
												'">'
										);
								iframe.document.close();
							} else if (
								object.type == 'video' ||
								object.type == 'link'
							) {
								$('#mhLightbox_object').attr('src', object.href);
							}
							$('#mhLightbox_container').fadeIn(500);
						});
					}
					pluginActive = true;
					toggleControls(objectGroupKey, objects[objectGroup].length);
				}
			});
			var touchStart = {};
			var touchEnd = {};
			$(document).on('touchstart', function(e) {
				if (pluginActive) {
					var touchObject = e.touches[0];
					var touchObjectTarget = touchObject.target;
					if (
						!$(touchObjectTarget).is('#mhLightbox_wrapper_gallery') &&
						!$(touchObjectTarget).is('.mhLightbox_gallery_object')
					) {
						touchStart.x = touchObject.clientX;
						touchStart.y = touchObject.clientY;
						touchStart.time = Date.now();
					}
				}
			});
			$(document).on('touchmove', function(e) {
				if (pluginActive) {
					var touchObject = e.touches[0];
					var touchObjectTarget = touchObject.target;
					if (
						!$(touchObjectTarget).is('#mhLightbox_wrapper_gallery') &&
						!$(touchObjectTarget).is('.mhLightbox_gallery_object')
					) {
						touchEnd.x = touchObject.clientX;
						touchEnd.y = touchObject.clientY;
						touchEnd.time = Date.now();
					}
				}
			});
			$(document).on('touchend', function(e) {
				if (pluginActive) {
					if ($.isEmptyObject(touchEnd)) {
						touchEnd.x = touchStart.x;
						touchEnd.y = touchStart.y;
						touchEnd.time = touchStart.time;
					}
					var touchDirection = 'previous';
					if (touchEnd.x < touchStart.x) touchDirection = 'next';
					if (
						(touchEnd.x - touchStart.x >= 30 ||
							touchStart.x - touchEnd.x >= 30) &&
						touchEnd.y - touchStart.y <= 100 &&
						touchStart.y - touchEnd.y <= 100 &&
						touchEnd.time - touchStart.time <= 2500
					) {
						move(touchDirection);
					}
					touchStart = {};
					touchEnd = {};
				}
			});
			$(document).on('click', '#mhLightbox_prev:not(.disabled)', function(
				e
			) {
				e.preventDefault();
				move('previous');
				return false;
			});
			$(document).on('click', '#mhLightbox_next:not(.disabled)', function(
				e
			) {
				e.preventDefault();
				move('next');
				return false;
			});
			$(document).on('click', '#mhLightbox_img_edit_rotate', function(
				e
			) {
				e.preventDefault();
				rotate();
				return false;
			});
			$(document).on('click', '#mhLightbox_img_edit_flip', function(
				e
			) {
				e.preventDefault();
				flip();
				return false;
			});
			$(document).on('click', '#mhLightbox_zoom_icon_plus:not(.disabled)', function(
				e
			) {
				e.preventDefault();
				zoom('plus');
				return false;
			});
			$(document).on('click', '#mhLightbox_zoom_icon_minus:not(.disabled)', function(
				e
			) {
				e.preventDefault();
				zoom('minus');
				return false;
			});
			$(document).on('mousedown', '#mhLightbox_object.dragable', function(
				e
			) {
				e.preventDefault();
				dragInit(e);
				return false;
			});
			$(document).on('mousemove', '#mhLightbox_object.dragable', function(
				e
			) {
				e.preventDefault();
				drag(e, $(this));
				return false;
			});
			$(document).on('mouseup', '#mhLightbox_object.dragable', function(
				e
			) {
				e.preventDefault();
				dragEnd();
				return false;
			});
			$(document).on('keyup', function(e) {
				if (pluginActive) {
					e.preventDefault();
					if (e.keyCode === 37) {
						// left arrow
						move('previous');
					} else if (e.keyCode === 39) {
						// right arrow
						move('next');
					} else if (e.keyCode === 27) {
						// esc
						$('#mhLightbox_close').trigger('click');
					}
					return false;
				}
			});
			$(document).on('click', '#mhLightbox_close, #mhLightbox_container', function(e) {
				e.preventDefault();
				if(!dragActive) {
					$.each(objects, function(indexGroup, objectGroup) {
						objects[indexGroup].active = false;
						$.each(objectGroup, function(indexGroupKey, objectGroupKey) {
							objects[indexGroup][indexGroupKey].active = false;
						});
					});
					pluginActive = false;
					$('#mhLightbox_container').fadeOut(500, function() {
						$('#mhLightbox_wrapper').css({
							'width': '',
							'height': '',
						});
						$('#mhLightbox_wrapper_gallery').html('');
						$('#mhLightbox_object').attr('src', '');
					});
				} else {
					dragEnd(true);
				}
			});
			$(document).on('click', '#mhLightbox_wrapper', function(e) {
				e.preventDefault();
				return false;
			});
			$(document).on('click', '#mhLightbox_object_subtitle a', function(
				e
			) {
				e.preventDefault();
				window.open($(this).attr('href'), '_blank');
				return false;
			});
			$(document).on('click', '.mhLightbox_gallery_object', function(e) {
				e.preventDefault();
				var galleryObject = $(this);
				var galleryObjectGroup = galleryObject.attr(
					'mhlightboxgallerygroup'
				);
				var galleryObjectGroupKey = parseInt(
					galleryObject.attr('mhlightboxgallerygroupind')
				);
				$.each(objects[galleryObjectGroup], function(index, object) {
					objects[galleryObjectGroup][index].active = false;
				});
				$('.mhLightbox_gallery_object.active').removeClass('active');

				var object = objects[galleryObjectGroup][galleryObjectGroupKey];
				object.active = true;
				galleryObject.addClass('active');
				var galleryObjectTitle = object.title;
				if (object.type == 'image')
					galleryObjectTitle = object.img.title;

				if (object.type == 'image') {
					var objectActiveTitle =
						object.img.title !== '' &&
						typeof object.img.title !== 'undefined'
							? object.img.title
							: object.title !== '' &&
							  typeof object.title !== 'undefined'
							? object.title
							: object.alt !== '' &&
							  typeof object.alt !== 'undefined'
							? object.alt
							: '';
					objectActive = new Image();
					objectActive.src = object.href;
					objectActive.title = objectActiveTitle;
					objectActive.type = object.type;
					$('#mhLightbox_object_count').html(
						parseInt(galleryObjectGroupKey) +
							1 +
							' / ' +
							objects[galleryObjectGroup].length
					);
					$('#mhLightbox_object_subtitle').html(galleryObjectTitle);
					objectActive.onload = function() {
						animate(objectActive);
						$('#mhLightbox_container').fadeIn(500);
					};
				} else {
					objectActive = {};
					objectActive.type = object.type;
					$('#mhLightbox_object').remove();
					$('#mhLightbox_wrapper').prepend(
						"<iframe id='mhLightbox_object' frameborder='0'></iframe>"
					);
					$('#mhLightbox_object_count').html(
						parseInt(galleryObjectGroupKey) +
							1 +
							' / ' +
							objects[galleryObjectGroup].length
					);
					resize({ type: object.type }, function() {
						if (object.type == 'content') {
							var iframe = document.getElementById(
								'mhLightbox_object'
							);
							iframe =
								iframe.contentWindow ||
								iframe.contentDocument.document ||
								iframe.contentDocument;
							iframe.document.open();
							iframe.document.write($('' + object.href).html());
							if (_settings.inlineCSS !== '')
								$('#mhLightbox_object')
									.contents()
									.find('head')
									.append(
										'<link rel="stylesheet" href="' +
											_settings.inlineCSS +
											'">'
									);
							iframe.document.close();
						} else if (
							object.type == 'video' ||
							object.type == 'link'
						) {
							$('#mhLightbox_object').attr('src', object.href);
						}
						$('#mhLightbox_container').fadeIn(500);
					});
				}
				toggleControls(
					galleryObjectGroupKey,
					objects[galleryObjectGroup].length
				);
				return false;
			});
			$(window).on('resize', function() {
				browserWidth = $(window).width();
				browserHeight = $(window).height();
				if (objectActive !== '') {
					resize(objectActive);
				}
			});
		}

		// start the plugin
		createStructur();
		bind();
		init();
		$(document).ajaxComplete(function() {
			init();
		});

		// self retun
		return this;
		// --- ENDE
	};
})(jQuery);
