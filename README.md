# mhLightbox
Responsive jQuery Bilder, Video und Content-Lightbox

Dieses Javascript-Plugin für Webseiten ermoeglicht einen Aufruf kleinerer Bilder (Thumbnails) für eine grössere Darstellung.
Das Plugin ist im Responsive-Design aufgebaut und ermöglicht eine volle Funktionalität auf allen Geräten.

# Changelog
V 3.9
  - Fehlerbehebung Event Listener

V 3.8
  - falsche Berechnung der Breite und Höhe des Bildes behoben
  
V 3.7
  - Bugs behoben

V 3.6
  - Fehlerbehebungen Bildbearbeitung
  - Bugs behoben

V 3.5 - Bildbearbeitung
  - neuer Punkt unter Einstellungen / Settings: imageEditActions (boolean)
  - zoomen, drehen und spiegeln von Bildern (nach Aktivierung)

V 3.4 - WebP Support
  - WebP Bild-Format wird jetzt unterstützt

V 3.3 - Fehlerbehebungen
  - Einzelbild-Bug behoben

V 3.2 - Fehlerbehebungen
  - Bugs behoben

V 3.1 - inlineCSS
  - neuer Parameter: inlineCSS
    * fügt ein &lt;link rel='stylesheet' href='inlineCSS'&gt; in das iFrame Modal ein

V 3.0 - Relaunch
  - neue Programmierung
    * Ajax Ready: Neuinitialisierung nach Ajax-Aufruf von Seiten
    * jQuery Element unabhängig
    * mehr Parameter
    * Callback-Funktionalität
  - neues Design
    * schlichter und schneller dank CSS3
    * erhöhte Kompatibilität

# Installation / Usage
- Download der Dateien mhLightbox.js / mhLightbox.css oder mhLightbox.min.js / mhLightbox.min.css
- Einbinden in den Quellcode (HTML): (Gegebenenfalls die Pfade zu den Dateien anpassen)

```html
<!-- In den <head></head> -->
<link rel="stylesheet" href="mhLightbox.css">
<script src="mhLightbox.js"></script>
```

```html
<!-- Vor </body> -->
<script>$.mhLightbox();</script>
```

- Einstellungen / Settings

```html
<script>
$.mhLightbox({
    selector: '[mhlightbox]',                   // jQuery element-selector
    imageTypes: "jpg|jpeg|gif|png|svg|webp",    // choose which image-types to allow
    imageMaxWidth: null,                        // (int) | null=>default
    imageMaxHeight: null,                       // (int) | null=>default
    imageEditActions: false,                    // (boolean) | false=>default
    padding: "80px",                            // (int) | px | %
    inlineCSS: "",                            	// (string) | relative path to a css file
    initCallback: function(objects){},          // array of all objects
    moveCallback: function(direction){}         // previous, next
});
</script>
```

- Struktur:

```html
<!-- Alle Attribute -->
<a href="Bild | externer Link | Youtube-Video | Div-ID" mhlightbox="Gruppenname" title="Titel">
  <img src="thumbnaillink.jpg" alt="Alternativtext" title="Bildunterschrift" width="250"> oder Textlink für Youtube|externe Links|Div-Inhalte
</a>

<!-- Bild -->
<a href="bildname_gross.jpg" mhlightbox="Gruppenname" title="Titel">
  <img src="bildname_klein.jpg" alt="Alternativtext" title="Bildunterschrift" width="250">
</a>

<!-- externer Link -->
<a href="https://www.hostname.tld" mhlightbox="Gruppenname" title="Titel">Link zu hostname.tld</a>

<!-- Youtube-Video -->
<a href="https://www.youtube.com/watch?v=XXXXXXXXXXX" mhlightbox="Gruppenname" title="Titel">Youtube-Video</a>

<!-- Div-Inhalte -->
<a href="#meinDiv" mhlightbox="Gruppenname" title="Titel">Div-Inhalt</a>
<div id="meinDiv">
    <h1>Mein Inhalt für die Lightbox</h1>
    <p>und allen Elementen aus HTML</p>
</div>
```

# Kompatibilität
  - Chrome
  - Firefox
  - Opera
  - Internet Explorer >= Version 9
